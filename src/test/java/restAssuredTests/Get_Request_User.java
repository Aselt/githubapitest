package restAssuredTests;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Get_Request_User {

  /*
  This request searching with user query by name 'mojombo'
   */
  @Test(priority = 1)
  public void getUserDetails(){
    given()
            .header("accept","application/vnd.github.v3+json")
            .queryParam("q", "mojombo")
       .when()
             .get("https://api.github.com/search/users")
            .then()
            .statusCode(200)
            .header("Content-Type", "application/json; charset=utf-8")
            .assertThat().body("items[0].login", equalTo("mojombo"));
  }

  /*
  This Request with author-name, committer-name 'wanstrath', and committer-date '2008-08-02
   */

  @Test(priority = 2)
  public void getUserCommit(){
    given()
            .header("accept", "application/vnd.github.cloak-preview+json")
            .queryParam("q", "committer-name:wanstrath")
            .queryParam("q", "committer-date:2008-08-02")
            .queryParam("q", "author-name:wanstrath")
            .when()
            .get("https://api.github.com/search/commits")
            .then()
            .statusCode(200)
            .header("Content-Type", "application/json; charset=utf-8")
            .assertThat().body("items[0].commit.author.name	", equalTo("Chris Wanstrath"));
  }

  /*
  Here I want to find the definition of the 'addClass' function inside 'jQuery' repository, language is JS
   */
  @Test(priority = 3)
  public void getReqCodeByRepo(){
    given()
            .urlEncodingEnabled(false)
            .header("accept", "application/vnd.github.v3+json")
            .queryParam("q", "addClass+in:file+language:js+repo:jquery/jquery")
            .when()
            .get("https://api.github.com/search/code")
            .then()
            .statusCode(200)
            .header("Content-Type", "application/json; charset=utf-8")
            .body("items[0].path", equalTo("src/attributes/classes.js"));
  }

  /*
  This request search Code by user and repo 'GoogleChrome/lightouse' with file extension '.js'
  Status code: 403 rate limit exceeded
   */
  @Test(priority = 4)
  public void getReqCodeByUser(){
    given()
            .header("accept", "application/vnd.github.v3+json")
            .log().all()
            .queryParam("q", "repo:GoogleChrome/lightouse")
            .queryParam("q", "user:paulirish extension:js")
            .when()
            .get("https://api.github.com/search/code")
            .then()
            .statusCode(200)
            .header("Content-Type", "application/json; charset=utf-8")
            .body("items[0].repository.owner.login", equalTo("paulirish"));

  }


  /*
  This query searches issues labeled as 'bug', author 'gitorikian', involves 'defunkt' etc.
   */
  @Test(priority = 5)
  public void getReqIssueByAuthor(){
    given()
            .header("accept", "application/vnd.github.v3+json")
            .queryParam("q", "github commenter:defunkt type:issue")
            .queryParam("q", "is:issue label:bug is:closed")
            .queryParam("q", "involves:defunkt involves:jlord")
            .queryParam("q", "cool author:gjtorikian ")
            .when()
            .get("https://api.github.com/search/issues")
            .then()
            .statusCode(200)
            .header("Content-Type", "application/json; charset=utf-8")
            .body("items[0].user.login", equalTo("gjtorikian"));
  }

  /*
  This request finding bug which is closed, and sorted, ordered by asc, on 'cherrypy/cherrypy' repo
   */
  @Test(priority = 6)
  public void getReqIssueByClosedBug(){
    given()
            .header("accept", "application/vnd.github.v3+json")
            .queryParam("q", "label:bug+is:closed+language:python+state:open")
            .queryParam("sort", "created")
            .queryParam("order", "asc")
            .queryParam("q", "repo:cherrypy/cherrypy")
            .when()
            .get("https://api.github.com/search/issues")
            .then()
            .statusCode(200)
            .header("Content-Type", "application/json; charset=utf-8")
            .body("items[0].repository_url", equalTo("https://api.github.com/repos/cherrypy/cherrypy"))
            .body("items[0].labels[1].name", equalTo("bug"));
  }


  /*
  This query searches for repository 'GoogleChrome/lighthouse' with repository_id '219553855' and user 'paulirish'
   */
  @Test(priority = 7)
  public void getReqLabels(){
    given()
            .header("accept", "application/vnd.github.v3+json")
            .queryParam("repository_id", "219553855")
            .queryParam("q", "repo:GoogleChrome/lighthouse")
            .queryParam("q", "user:paulirish")
            .when()
            .get("https://api.github.com/search/labels")
            .then()
            .statusCode(200)
            .header("Content-Type", "application/json; charset=utf-8")
            .body("items[3].name", equalTo("bug"))
            .body("items[3].id", equalTo(1657103420));
  }

  /*
   This query searches for repository 'GoogleChrome/lighthouse' with only five forks, and user 'symfony'
   */
  @Test(priority = 8)
  public void getReqRepoFork(){
    given()
            .header("accept", "application/vnd.github.v3+json")
            .queryParam("q", "repo:GoogleChrome/lighthouse")
            .queryParam("q", "forks:5")
            .queryParam("q", "user:symfony")
            .when()
            .get("https://api.github.com/search/repositories")
            .then()
            .statusCode(200)
            .header("Content-Type", "application/json; charset=utf-8")
            .body("items[0].name", equalTo("symfony"));
  }


//This query searches for topics with the keyword 'ruby'
  @Test(priority = 9)
  public void getReqTopics(){
    given()
            .header("accept", "application/vnd.github.mercy-preview+json")
            .queryParam("q", "ruby")
            .when()
            .get("https://api.github.com/search/topics")
            .then()
            .statusCode(200)
            .header("Content-Type", "application/json; charset=utf-8")
            .body("items[0].created_by", equalTo("Yukihiro Matsumoto"));
  }

  /*
  This query searches for users with the name 'tom'. The results are restricted to users with more
  than 42 repositories and over 1000
   */
  @Test(priority = 10)
  public void getReqUserByFollowers(){
    given()
            .urlEncodingEnabled(false)
            .header("accept","application/vnd.github.v3+json")
            .header("accept", "application/vnd.github.v3.text-match+json")
            .queryParam("q", "tom+repos:%3E42+followers:%3E1000")
            .when()
            .get("https://api.github.com/search/users")
            .then()
            .statusCode(200)
            .header("Content-Type", "application/json; charset=utf-8")
            .assertThat().body("items[0].login", equalTo("mojombo"));
  }


}
