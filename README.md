## Github Search Rest Api Testing
This project testing [github search rest api](https://docs.github.com/en/rest/reference/search#search-repositories) with rest assured library, build on Java.
For package management and dependencies used Maven, and for assertion TestNG.

### pom.xml maven plugins, and api dependencies
- Surefiere Plugin 
- rest assured
- json-schema-validator
- json-path
- xml-path
- testng
- java-hamcrest
- hamcrest-core
- hamcrest-library

## Install
From Gitlab:
```
$ git clone https://gitlab.com/Aselt/githubapitest.git
```

## Running
1. From package, inside src/test/java/restAssuredTests has a class Get_Request_User implemented methods, we can run each method.
2. Using testng.xml, inside project created **testng.xml**, we can use this file running all methods same time
3. Using pom.xml, inside project created **pom.xml**, using Maven cli or code editor maven tool
```
$ mvn clean install
or
$ mvn test
```


### Issue
Sometimes when we run all methods from testng.xml, on console we can get *422 Unprocessable Entity* . The reason might be [Rate Limit](https://docs.github.com/en/rest/reference/search#search-repositories). 